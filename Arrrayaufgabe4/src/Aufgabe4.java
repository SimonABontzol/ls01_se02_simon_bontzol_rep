
public class Aufgabe4 {

	/*
	 * public static void main(String[] args) { // TODO Auto-generated method stub
	 * boolean pr�fung = false; boolean pr�fung2 = false; int[] lottozahlen = { 3,
	 * 7, 12, 18, 37, 42 }; for (int i = 0; i < lottozahlen.length; i++) {
	 * System.out.print(lottozahlen[i] + " "); }
	 * 
	 * System.out.println(" "); int z�hler = 0;
	 * 
	 * while (z�hler < lottozahlen.length && pr�fung == false) { if
	 * (lottozahlen[z�hler] != 12) { pr�fung = false; z�hler++; } else { pr�fung =
	 * true; z�hler++; }
	 * 
	 * } int z�hler2 = 0; while (z�hler2 < lottozahlen.length && pr�fung2 == false)
	 * { if (lottozahlen[z�hler2] != 13) { pr�fung2 = false; z�hler2++; } else {
	 * pr�fung2 = true; z�hler2++; }
	 * 
	 * }
	 * 
	 * if (pr�fung == true) {
	 * System.out.println("Die Zahl 12 ist in der Ziehung enthalten."); } else {
	 * System.out.println("Die Zahl 12 ist in der Ziehung nicht enthalten."); }
	 * 
	 * if (pr�fung2 == true) {
	 * System.out.println("Die Zahl 13 ist in der Ziehung enthalten."); } else {
	 * System.out.println("Die Zahl 13 ist in der Ziehung nicht enthalten."); }
	 * 
	 * }
	 * 
	 * }
	 */ 

	public static void main(String[] args) {

		int[] lottozahlen = { 3, 7, 12, 18, 37, 42 };
		for (int i = 0; i < lottozahlen.length; i++) {
			System.out.print(lottozahlen[i] + " ");
		}
		System.out.println(" ");
		lottotest(lottozahlen, 12);
		lottotest(lottozahlen, 13);
	}

	public static void lottotest(int[] lottozahlen, int pr�fzahl) {
		int z�hler = 0;
		boolean pr�fung = false;
		while (z�hler < lottozahlen.length && pr�fung == false) {
			if (lottozahlen[z�hler] == pr�fzahl) {
				pr�fung = true;
				z�hler++;
			} else {
				pr�fung = false;
				z�hler++;
			}
		}
		if (pr�fung == true) {
			System.out.println("Die Zahl " + pr�fzahl + " ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl " + pr�fzahl + " ist in der Ziehung nicht enthalten.");
		}

	}
}
