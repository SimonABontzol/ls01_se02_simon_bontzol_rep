import java.util.Scanner;

public class ZRechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Laufzeit (in Jahren) des Sparvertrags: ");
		int laufzeit = tastatur.nextInt();
		
		System.out.println("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
		double kapital = tastatur.nextDouble();
		
		System.out.println("Zinssatz ");
		double zinssatz = tastatur.nextDouble();
		
		int zaehler = 0;
		
		System.out.println(" ");
		System.out.printf("%s %.2f %s \n" , "Eingezahltes Kapital: " , kapital , "�");
		
		while(zaehler < laufzeit) {
			
			double prozentsatz = zinssatz/100.0;
			kapital = kapital + (kapital * prozentsatz);
			zaehler++;
			
		}
		System.out.printf( "%s %.2f %s \n" , "Ausgezahltes Kapital: " , kapital , "�");
	}

}