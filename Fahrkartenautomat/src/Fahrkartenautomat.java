﻿import java.util.Scanner;

class Fahrkartenautomat {
	
	public static void main(String[] args) {
		boolean unlimitedruns = true;
		int[] muenzkasse = { 5, 5, 5, 5, 5, 5, 5 }; // 2; 1; 0.5; 0.2; 0.1; 0.05; 0.01
		while (unlimitedruns) {
			double euroCentBetrag;
			double gesamtpreis;
			int[] muenzwert = { muenzkasse[0] * 200, muenzkasse[1] * 100, muenzkasse[2] * 50, muenzkasse[3] * 20,
					muenzkasse[4] * 10, muenzkasse[5] * 5, muenzkasse[6] * 1 };
			// start_pause
			// Gesamtpreis wird berechnet
			gesamtpreis = fahrkartenbestellungErfassen();

			// Geldeinwurf
			euroCentBetrag = fahrkartenBezahlen(gesamtpreis, muenzwert);

			// Fahrscheinausgabe
			fahrkartenAusgeben("Fahrschein wird ausgegeben");

			// Rückgeldberechnung und -Ausgabe
			rueckgeldAusgeben(gesamtpreis, euroCentBetrag, muenzkasse);

			// Console Reset

		}
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double ticketpreis = 0;

		// Kartenpreis wird festgelegt

		System.out.printf("Fahrkartenbestellvorgang:\n" + "=========================\n\n");
		/* Länge: 10 */ double[] fahrkartenpreis = { 2, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.30, 24.9 };
		/* Länge: 10 */ String[] fahrkartenbezeichnung = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };
		System.out.println("Fahrkartenauswahl:");
		System.out.printf("%-50s%-50s%-50s\n", "Auswahlnummer", "Bezeichnung", "Preis in Euro");
		for (int i = 0; i < fahrkartenpreis.length; i++) {
			System.out.printf("%-50d%-50s%-50s\n", i + 1, fahrkartenbezeichnung[i], fahrkartenpreis[i]);
		}

		System.out.println(" ");
		System.out.println("Wählen sie eine Fahrkarte aus: ");
		int auswahl = tastatur.nextInt() - 1;
		ticketpreis = fahrkartenpreis[auswahl];
		System.out.println("Sie haben folgende Fahrkarte ausgewählt: " + fahrkartenbezeichnung[auswahl]);

		// Kartenanzahl wird festgelegt
		System.out.println(" ");
		System.out.print("Kartenanzahl: ");
		System.out.println(" ");
		int kartenAnzahl = tastatur.nextInt();

		while (kartenAnzahl < 1 || kartenAnzahl > 10) {
			kartenAnzahl = 0;
			System.out.println(
					"Der eingegebene Wert kann nicht ausgegeben werden. Bitte geben sie einen Wert zwischen 1 und 10 ein");
			System.out.print("Kartenanzahl: ");
			System.out.println(" ");
			kartenAnzahl = tastatur.nextInt();
		}
		// Gesamtpreis wird berechnet
		double gesamtpreis = ticketpreis * kartenAnzahl;

		return gesamtpreis;
	}

	public static double fahrkartenBezahlen(double gesamtpreis, int[] muenzwert) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		double euroCent;
		while (eingezahlterGesamtbetrag < gesamtpreis) {
			System.out.printf("%s %.2f %s", "Noch zu zahlen:", gesamtpreis - eingezahlterGesamtbetrag, "EURO ");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		int kassenwert = muenzwert[0] + muenzwert[1] + muenzwert[2] + muenzwert[3] + muenzwert[4] + muenzwert[5]
				+ muenzwert[6];
		if (eingezahlterGesamtbetrag > kassenwert) {
			System.out.println("Es ist leider nicht genug Wechselgeld vorhanden. Haben sie noch einen schönen Tag.");
			System.exit(0);
		}
		// Eingezahlter Betrag wird in Cent umgerechnet
		euroCent = (eingezahlterGesamtbetrag * 100);
		return euroCent;
	}

	public static void fahrkartenAusgeben(String ausgabe) {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\n" + ausgabe);
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static int[] rueckgeldAusgeben(double gesamtpreis, double euroCentBetrag, int[] muenzkasse) {
		double rückgabebetrag = ((gesamtpreis * 100) - euroCentBetrag) * -1;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag / 100 + " EURO ");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 200) // 2 EURO-Münzen
			{
				geldRückgabe(200);
				rückgabebetrag -= 200;
			}
			while (rückgabebetrag >= 100) // 1 EURO-Münzen
			{
				geldRückgabe(100);
				rückgabebetrag -= 100;
			}
			while (rückgabebetrag >= 50) // 50 CENT-Münzen
			{
				geldRückgabe(50);
				rückgabebetrag -= 50;
			}
			while (rückgabebetrag >= 20) // 20 CENT-Münzen
			{
				geldRückgabe(20);
				rückgabebetrag -= 20;
			}
			while (rückgabebetrag >= 10) // 10 CENT-Münzen
			{
				geldRückgabe(10);
				rückgabebetrag -= 10;
			}
			while (rückgabebetrag >= 5)// 5 CENT-Münzen
			{
				geldRückgabe(5);
				rückgabebetrag -= 5;
			}
			while (rückgabebetrag >= 1)// 1 CENT-Münzen
			{
				geldRückgabe(1);
				rückgabebetrag -= 1;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n");
		return muenzkasse;
	}

	public static void geldRückgabe(int wert) {
		String währung;
		if (wert >= 100) {
			währung = "Euro";
			wert = wert / 100;
		} else {
			währung = "Cent";
		}
		System.out.printf("%8s\n", "* * *");
		System.out.printf("%2s%8s\n", "*", "*");
		System.out.printf("%1s%5d%5s\n", "*", wert, "*");
		System.out.printf("%1s%7s%3s\n", "*", währung, "*");
		System.out.printf("%2s%8s\n", "*", "*");
		System.out.printf("%8s\n", "* * *");
	}
}
