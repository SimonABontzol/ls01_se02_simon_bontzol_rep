import java.util.Scanner;

public class whileSchleife {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner tastatur = new Scanner(System.in);
		boolean abfrage = true;
		String abfrageBuchstabe;

		while (abfrage == true) {

			System.out.print("Geben Sie eine Zahl ein: ");

			int eingabeZahl = tastatur.nextInt();
			int vergleichsZahl = 0;

			while (eingabeZahl > vergleichsZahl) {
				vergleichsZahl++;
				System.out.print(" " + vergleichsZahl);
			}

			System.out.println(" ");

			for (int i = 0; i < 8; i++) {
				System.out.print(".");
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			System.out.println(" ");

			System.out.println("Wenn Sie es wiederholen wollen geben Sie j ein: ");

			abfrageBuchstabe = tastatur.next();

			if (!abfrageBuchstabe.equals("j")) {
				abfrage = false;
			}

		}
	}

}
