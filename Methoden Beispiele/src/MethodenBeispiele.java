import java.util.Scanner;

public class MethodenBeispiele {

	public static void main(String[] args) {
			
		Scanner myScanner = new Scanner(System.in);
		
		//System.out.print("Geben Sie bitte Ihren Namen ein: ");
		// String name = myScanner.next();
		
		String vname = leseString("Geben Sie bitte Ihren Vornamen ein: ");
		String nname = leseString("Geben Sie bitte Ihren Nachnamen ein: ");
		int alter = leseInt("Geben Sie bitte Ihr Alter ein: ");
		
		sayHello(vname, nname);
		System.out.println("Sie sind " + alter + " Jahre alt.");		
		
	}
	
	public static void sayHello(String vname, String nname) {
		
		System.out.print("Hallo " + vname + " " + nname + ". ");
		
	}
	
    public static int leseInt(String text2) {
		
		System.out.print(text2);
		Scanner myScanner= new Scanner(System.in);
		
		int alter = myScanner.nextInt();
		
		return alter;
		
    }
	
	public static String leseString(String text) {
		
		System.out.print(text);
		Scanner myScanner= new Scanner(System.in);
		
		String str = myScanner.next();
		
		return str;
		
	}
	
}
