
public class Ladung {
	private String name;
	private int anzahl;
	
	public Ladung() {
		this.name = "Unbekannt";
		this.anzahl = 0;
	}
	
	public Ladung(String name, int anzahl) {
		this.name = name;
		this.anzahl = anzahl;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setAnzahl(int alter) {
		this.anzahl = anzahl;
	}

	public int getAnzahl() {
		return anzahl;
	}
}
