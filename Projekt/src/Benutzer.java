
public class Benutzer {
	private int benutzernummer;
	private String vorname;
	private String nachname;
	private int geburtsjahr;

	public Benutzer() {
		this.benutzernummer = 0;
		this.vorname = "Unbekannt";
		this.nachname = "Unbekannt";
		this.geburtsjahr = 0;
	}

	public Benutzer(int benutzernummer, String vorname, String nachname, int geburtsjahr) {
		this.benutzernummer = benutzernummer;
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsjahr = geburtsjahr;
	}

	public void setbenutzernummer(int benutzernummer) {
		this.benutzernummer = benutzernummer;
	}

	public int getbenutzernummer() {
		return benutzernummer;
	}

	public void setvorname(String vorname) {
		this.vorname = vorname;
	}

	public String getvorname() {
		return vorname;
	}

	public void setnachname(String nachname) {
		this.nachname = nachname;
	}

	public String getnachname() {
		return nachname;
	}

	public void setgeburtsjahr(int geburtsjahr) {
		this.geburtsjahr = geburtsjahr;
	}

	public int getgeburtsjahr() {
		return geburtsjahr;
	}

	public String toString() {
		return "Benutzer-ID: " + this.benutzernummer + " | Vorname: " + this.vorname + " | Nachname: " + this.nachname
				+ " | Geburtsjahr " + this.geburtsjahr;
	}

}