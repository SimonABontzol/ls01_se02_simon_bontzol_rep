import java.util.Arrays;
public class ArrayHelper {

	public static void main(String[] args) {
		int[] testliste = { 1, 2, 3, 4, 5 };
		String zahlenauflistung;
		zahlenauflistung=convertArrayToString(testliste);
		System.out.println(zahlenauflistung);
		
	}

	public static String convertArrayToString(int[] zahlen) {
		String auflistung = Arrays.toString(zahlen);
		return auflistung;
	}

}
