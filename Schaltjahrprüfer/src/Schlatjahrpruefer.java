import java.util.Scanner;

public class Schlatjahrpruefer {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Geben Sie eine Jahreszahl ein: ");
		int jahreszahl = tastatur.nextInt();
		
		if (jahreszahl <= 1582) {
			
			if (jahreszahl % 4 == 0) {
				System.out.println(jahreszahl + " ist ein Schlatjahr");
			}
			else {
				System.out.println(jahreszahl + " ist kein Schlatjahr");
			}
		}
		else if (jahreszahl % 4 == 0) {
			
			if (jahreszahl % 100 == 0) {
				
				if (jahreszahl % 400 == 0) {
					System.out.println(jahreszahl + " ist ein Schlatjahr");
				}
				else {
					System.out.println(jahreszahl + " ist kein Schlatjahr");
				}
				
			}
			else {
				System.out.println(jahreszahl + " ist ein Schlatjahr");
			}
		}
		else {
			System.out.println(jahreszahl + " ist kein Schlatjahr");
		}

	}

}
