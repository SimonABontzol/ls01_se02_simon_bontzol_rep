/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 150000000000l;
    
    // Wie viele Einwohner hat Berlin?
    long bewohnerBerlin = 3800000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 6779;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    long gewichtKilogramm = 190000;  
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    long flaecheGroessteLand = 17098242;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    double flaecheKleinsteLand = 0.44; 
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: "+anzahlSterne);
    
    System.out.println("Anzahl der Einwohner in Berlin: "+bewohnerBerlin);
    
    System.out.println("Wie alt ich bin: "+alterTage);
    
    System.out.println("Gewicht des Schwersten Tieres: "+gewichtKilogramm);
    
    System.out.println("Fl�che des gr��ten Landes "+flaecheGroessteLand);
    
    System.out.println("Fl�che des kleinsten Landes "+flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}