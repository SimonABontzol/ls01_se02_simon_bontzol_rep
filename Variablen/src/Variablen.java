/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	  int zaehler;

    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	        zaehler = 25;
	  		System.out.println(zaehler +" Durchl�ufe");
    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
	  String menuauswahl;	

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	       menuauswahl = "C";
	       System.out.println(menuauswahl);
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	   long lichtgeschwindigkeit;   

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	   		lichtgeschwindigkeit = 299792;
	   		System.out.println("Die Lichtgeschwindigkeit betr�gt "+ lichtgeschwindigkeit +"Kilometer pro Sekunde");	

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	   	int	anzahlMitglieder = 7;
	   		
	   	
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	   	System.out.println("Es gibt " + anzahlMitglieder+ " Vereinsmitglieder." );

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	   String elementarladung="1,602x10^-19 As";
	   	System.out.println("Die Elementarladung betr�gt rund "+elementarladung + ".");

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	   	boolean erfolgteZahlung;

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	   		erfolgteZahlung=true;
	   		if(erfolgteZahlung=true) {
	   			System.out.println("Die Zahlung ist erfolgt");
	   		}
	   				

  }//main
}// Variablen