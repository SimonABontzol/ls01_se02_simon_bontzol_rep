import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		

		// Benutzereingaben lesen
		String artikel = leseString("Was m�chten Sie bestellen? ");
		
		int anzahl = leseInt("Geben Sie die Anzahl ein: ");

		double preis = leseDouble("Geben Sie den Nettopreis ein: ");

		double mwst = leseDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");
			 
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
		}
	
		public static String leseString(String text) {
			
			System.out.println(text);
			Scanner myScanner= new Scanner(System.in);
			
			String txt = myScanner.next();
			
			return txt;
			
	    }
		
		public static int leseInt(String text) {
			
			System.out.print(text);
			Scanner myScanner= new Scanner(System.in);
			
			int zhl = myScanner.nextInt();
			
			return zhl;
			
	    }
		
		public static double leseDouble(String text) {
			
			System.out.print(text);
			Scanner myScanner= new Scanner(System.in);
			
			double kzhl = myScanner.nextDouble();
			
			return kzhl;
			
	    }
		
		public static double berechneGesamtnettopreis (int anzahl, double preis) {
			
			double nettogesamtpreis = anzahl * preis;
			return nettogesamtpreis;
		}
		
		public static double berechneGesamtbruttopreis (double nettogesamtpreis, double mwst) {
			
			double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
			return bruttogesamtpreis;
			
		}
		
		public static void rechnungausgeben (String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
			
			System.out.println("\n\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
			
		}

}
