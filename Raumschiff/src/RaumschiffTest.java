
public class RaumschiffTest {

	public static void main(String[] args) {

		Raumschiff r1 = new Raumschiff();
		Raumschiff r2 = new Raumschiff();
		Raumschiff r3 = new Raumschiff();

		reparaturtestAufrufen(r1, r2, r3);
		PhasertestAufrufen(r1, r2, r3);
		PhotonentorpedotestAufrufen(r1, r2, r3);
		PhaserTorpedoTestAufrufen(r1, r2, r3);
		addLadungTestAufrufen(r1, r2, r3);
		nachladeTestAufrufen(r1, r2, r3);

	}

//	Tests
	public static void datenFuellen(Raumschiff r1, Raumschiff r2, Raumschiff r3) {

		r1.setPhotonentorpedoAnzahl(1);
		r1.setEnergieversorgungInProzent(100);
		r1.setSchildeInProzent(100);
		r1.setHuelleInProzent(100);
		r1.setLebenserhaltungssystemeInProzent(100);
		r1.setSchiffsname("IKS Hegh'ta");
		r1.setAndroidenAnzahl(2);

		r2.setPhotonentorpedoAnzahl(3);
		r2.setEnergieversorgungInProzent(100);
		r2.setSchildeInProzent(100);
		r2.setHuelleInProzent(100);
		r2.setLebenserhaltungssystemeInProzent(100);
		r2.setSchiffsname("IRW Khazara");
		r2.setAndroidenAnzahl(2);

		r3.setPhotonentorpedoAnzahl(1);
		r3.setEnergieversorgungInProzent(80);
		r3.setSchildeInProzent(80);
		r3.setHuelleInProzent(50);
		r3.setLebenserhaltungssystemeInProzent(100);
		r3.setSchiffsname("Ni'Var");
		r3.setAndroidenAnzahl(5);
	}

	public static void zustandRaumschiffTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {

		r1.zustandRaumschiff();
		r2.zustandRaumschiff();
		r3.zustandRaumschiff();
	}

	public static void addLadungTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r1.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		r1.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));

		r2.addLadung(new Ladung("Borg-Schrott", 5));
		r2.addLadung(new Ladung("Rote Materie", 2));
		r2.addLadung(new Ladung("Plasma-Waffe", 50));

		r3.addLadung(new Ladung("Forschungssonde", 35));
		r3.addLadung(new Ladung("Photonentorpedo", 3));

	}

	public static void torpedoschussTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
//		r1.photonentorpedoSchiessen(r3);
//		r1.photonentorpedoSchiessen(r3);
//		r1.photonentorpedoSchiessen(r3);

//		r2.photonentorpedoSchiessen(r3);
//		r2.photonentorpedoSchiessen(r3);
//		r2.photonentorpedoSchiessen(r3);

		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r2);

	}

	public static void phaserschussTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r1.phaserkanoneSchiessen(r2);
		r1.phaserkanoneSchiessen(r2);
		r1.phaserkanoneSchiessen(r2);

		r2.phaserkanoneSchiessen(r1);
		r2.phaserkanoneSchiessen(r1);
		r2.phaserkanoneSchiessen(r1);

		r3.phaserkanoneSchiessen(r1);
		r3.phaserkanoneSchiessen(r1);
		r3.phaserkanoneSchiessen(r1);
	}

	public static void reparaturTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		// (Schilde, Energieversorgung, Huelle, Androiden, die man einsetzen will)
		r3.reparaturDurchfuehren(false, false, false, 5);
	}

	public static void nachladeTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r2);

		r3.photonentorpedosLaden(3);

		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r1);
		r3.photonentorpedoSchiessen(r2);

		r3.eintraegeLogbuchZurueckgeben();
	}

//	Testaufrufe
	public static void reparaturtestAufrufen(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		datenFuellen(r1, r2, r3);
		zustandRaumschiffTest(r1, r2, r3);
		reparaturTest(r1, r2, r3);
		zustandRaumschiffTest(r1, r2, r3);
	}

	public static void PhasertestAufrufen(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		datenFuellen(r1, r2, r3);
		zustandRaumschiffTest(r1, r2, r3);
		phaserschussTest(r1, r2, r3);
		zustandRaumschiffTest(r1, r2, r3);
	}

	public static void PhotonentorpedotestAufrufen(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		datenFuellen(r1, r2, r3);
		zustandRaumschiffTest(r1, r2, r3);
		torpedoschussTest(r1, r2, r3);
		zustandRaumschiffTest(r1, r2, r3);
	}

	public static void PhaserTorpedoTestAufrufen(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		datenFuellen(r1, r2, r3);
		zustandRaumschiffTest(r1, r2, r3);
		torpedoschussTest(r1, r2, r3);
		phaserschussTest(r1, r2, r3);
		zustandRaumschiffTest(r1, r2, r3);
	}

	public static void addLadungTestAufrufen(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		datenFuellen(r1, r2, r3);
		addLadungTest(r1, r2, r3);
	}

	public static void nachladeTestAufrufen(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		datenFuellen(r1, r2, r3);
		addLadungTest(r1, r2, r3);
		nachladeTest(r1, r2, r3);
	}
}
