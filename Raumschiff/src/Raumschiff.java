import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<Ladung> ladungsverzeichnis;
	private ArrayList<String> broadcastKommunikator;

	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.androidenAnzahl = 0;
		this.schiffsname = "N/A";
		this.ladungsverzeichnis = new ArrayList<>();
		this.broadcastKommunikator = new ArrayList<>();
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname,
			ArrayList<Ladung> ladungsliste, ArrayList<String> broadcastKommunikator) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.ladungsverzeichnis = ladungsliste;
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public ArrayList<Ladung> getLadungsVerzeichnis() {
		return ladungsverzeichnis;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void addLadung(Ladung ladung) {
		ladungsverzeichnis.add(ladung);
		System.out.println("Ladung hinzugef�gt: " + ladung);
	}

	public void photonentorpedoSchiessen(Raumschiff zielSchiff) {
		String waffenArt = "Photonentorpedo";
		if (photonentorpedoAnzahl >= 1) {
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			zielSchiff.treffer(waffenArt);
		} else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}

	public void phaserkanoneSchiessen(Raumschiff zielSchiff) {
		String waffenArt = "Phaserkanone";
		if (energieversorgungInProzent >= 50) {
			nachrichtAnAlle("Phaserkanone abgeschossen");
			energieversorgungInProzent = energieversorgungInProzent - 50;
			zielSchiff.treffer(waffenArt);
		} else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}

	public void treffer(String waffenArt) {
		if (getHuelleInProzent() <= 0) {
			System.out.println("Das Schiff ist bereits au�er gefecht");
		} else {
			System.out.println(getSchiffsname() + " wurde getroffen!");
			broadcastKommunikator.add(getSchiffsname() + " wurde getroffen mit " + waffenArt);
			if (getSchildeInProzent() > 0) {
				setSchildeInProzent(getSchildeInProzent() - 50);
			} else if (getSchildeInProzent() <= 0 && getHuelleInProzent() > 0) {
				setHuelleInProzent(getHuelleInProzent() - 50);
				setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);
			}
			if (getHuelleInProzent() <= 0) {
				setLebenserhaltungssystemeInProzent(0);
				nachrichtAnAlle("Die Lebenserhaltunssysteme wurden Zerst�rt!");
			}
		}
	}

	public void nachrichtAnAlle(String nachricht) {
		this.broadcastKommunikator.add(nachricht);
	}

	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return this.broadcastKommunikator;
	}

	public void photonentorpedosLaden(int anzahlTorpedos) {
		for (Ladung ladung : ladungsverzeichnis) {
			if (ladung.getBezeichnung().equals("Photonentorpedo")) {
				int anzahlEntnommen = ladung.ladungEntnehmen(anzahlTorpedos);
				photonentorpedoAnzahl += anzahlEntnommen;
				if (anzahlEntnommen > 0)
					System.out.println(anzahlEntnommen + " Photonentorpedos eingesetzt");
				ladungsverzeichnisAufraeumen();
				return;
			}
		}
		nachrichtAnAlle("-=*Click*=-");
	}

	public void reparaturDurchfuehren(boolean schildeBeschaedigt, boolean energieversorgungBeschaedigt,
			boolean huelleBeschaedigt, int angeforderteAndroiden) {
		int eingesetzteAndroiden = 0;
		int schadenZaehler = 0;

		if (schildeBeschaedigt) {
			schadenZaehler++;
		}
		if (energieversorgungBeschaedigt) {
			schadenZaehler++;
		}
		if (huelleBeschaedigt) {
			schadenZaehler++;
		}

		Random r = new Random();
		int tiefsteZahl = 0;
		int hoechsteZahl = 100;
		int resultat = r.nextInt(hoechsteZahl - tiefsteZahl) + tiefsteZahl;

		eingesetzteAndroiden = Math.min(angeforderteAndroiden, androidenAnzahl);

		int berechneteReparaturInProzent = 0;
		if (schadenZaehler > 0) {
			berechneteReparaturInProzent = (resultat * eingesetzteAndroiden) / schadenZaehler;
		}

		if (schildeBeschaedigt) {
			this.setSchildeInProzent(Math.min(berechneteReparaturInProzent + this.getSchildeInProzent(), 100));
		}
		if (huelleBeschaedigt) {
			this.setHuelleInProzent(Math.min(berechneteReparaturInProzent + this.getHuelleInProzent(), 100));
		}
		if (energieversorgungBeschaedigt) {
			this.setEnergieversorgungInProzent(
					Math.min(berechneteReparaturInProzent + this.getEnergieversorgungInProzent(), 100));
		}
	}

	public void zustandRaumschiff() {
		System.out.println("Schiff: " + getSchiffsname());
		System.out.println("Photonentorpedos: " + getPhotonentorpedoAnzahl());
		System.out.println("Zustand der Energieversorgung: " + getEnergieversorgungInProzent() + "%");
		System.out.println("Zustand der Schilde: " + getSchildeInProzent() + "%");
		System.out.println("Zustand der H�lle: " + getHuelleInProzent() + "%");
		System.out.println("Zustand der Lebenserhaltungssysteme: " + getLebenserhaltungssystemeInProzent() + "%");
		System.out.println("Anzahl der Androiden: " + getAndroidenAnzahl());
		ladungsverzeichnisAusgeben();
		nachrichtenAusgeben();
	}

	public void ladungsverzeichnisAusgeben() {
		System.out.println("Ladungsliste:");
		for (Ladung ladung : ladungsverzeichnis) {
			System.out.println("  " + ladung);
		}
	}

	public void nachrichtenAusgeben() {
		System.out.println("Nachrichten:");
		for (String nachricht : broadcastKommunikator) {
			System.out.println("  " + nachricht);
		}
	}

	public void ladungsverzeichnisAufraeumen() {
		ArrayList<Ladung> neueLadungsliste = new ArrayList<>();
		for (Ladung ladung : ladungsverzeichnis) {
			if (ladung.getMenge() > 0) {
				neueLadungsliste.add(ladung);
			}
		}
		ladungsverzeichnis = neueLadungsliste;
	}

}
